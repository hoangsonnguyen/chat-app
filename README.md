## ReactJS, Redux, Typescript, Firebase FCM
This is a real-time chat app using NodeJS and ReactJS
It’s accompanied with a NodeJs repo: https://gitlab.com/hoangsonnguyen/chat-app-api

## To test the app
Please open chrome, firefox browsers with two different accounts on your screen. Then you can start send messages and test it out.

http://localhost:8889

## install on Mac/Linux OS
```sh
npm install
```
please use sudo if necessary

## install on Windows OS
```sh
npm install
```

## start
```sh
npm start
```

### if you have any trouble after a while, please ping me at sonnguyeninslife@gmail.com