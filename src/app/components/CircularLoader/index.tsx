import * as React from 'react';
const style = require('./style.scss');

interface ICircularLoaderProps {
  size?: number;
};

const CircularLoader: React.SFC<ICircularLoaderProps> = ({size = 20}) => {
  return (
    <div style={{width: size + 'px'}} className={style['circular-loader']}>
      <svg className={style['circular-loader__inner']} viewBox="25 25 50 50">
        <circle
          className={style['circular-loader__path']} cx="50" cy="50" r="20" fill="none"
          strokeWidth="2" strokeMiterlimit="10"
        />
      </svg>
  </div>
  );
};

export { CircularLoader };
