import * as React from 'react';

const IS_BROWSER = !!process.env.BROWSER;
const IS_LOCAL = !!process.env.LOCAL;
const IS_PRODUCTION = process.env.NODE_ENV === 'production';
const API_URL = 'http://0.0.0.0:3000';

export {
  API_URL,
  IS_BROWSER,
  IS_LOCAL,
  IS_PRODUCTION,
}

export type ConfigContextProps = Partial<{
  readonly API_URL: string;
  readonly IS_BROWSER: boolean;
  readonly IS_PRODUCTION: boolean;
}>;

export const configContextInitialState = {
  API_URL,
  IS_BROWSER,
  IS_PRODUCTION,
};

export type WithGlobalConfigContext = ConfigContextProps;
export const GlobalConfigContext = React.createContext<ConfigContextProps>(configContextInitialState);
GlobalConfigContext.displayName = 'GlobalConfigContext';
