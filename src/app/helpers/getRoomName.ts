export const getRoomName = (members: any[], currentUID: string) => {
  if (!Array.isArray(members)) {
    return '';
  }

  const newMembers = members.map((member) => {
    if (member._id === currentUID) {
      return 'me';
    }
    return `${member.email}`;
  });
  return newMembers.join(', ');
};
