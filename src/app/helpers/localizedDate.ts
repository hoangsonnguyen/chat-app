export const months = [
  'jan', 'feb', 'mar', 'apr', 'may', 'jun',
  'jul', 'aug', 'sep', 'oct', 'nov', 'dec',
];

export const localizedDate = (date) => {
  const parsedDate = date ? new Date(date) : new Date() as Date;
  const day = parsedDate.getDate();
  const month = months[parsedDate.getMonth()];
  return `${month} ${day}`;
};
