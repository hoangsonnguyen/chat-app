export const reverseMap = (arr, func) => {
  if (!Array.isArray(arr)) {
    return null;
  }
  const newArray = [];
  for ( let index = arr.length - 1; index >= 0; index-- ) {
    newArray.push(func(arr[index], index));
  }
  return newArray;
};
