export const getTime = (timePoint) => {
  if (!timePoint) {
    return '';
  }

  const createdTime = new Date(timePoint).getTime();
  const currentUnixTime = Date.now();
  const date = (currentUnixTime - createdTime) / 1000;
  let time ;
  switch (true) {
    case date < 60:
      time = 'now';
      break;
    case date < 3600:
      time =  `${Math.floor(date / 60)} minutes ago`;
      break;
    case date < 3600 * 24:
      time = `${Math.floor(date / 3600)} hours ago`;
      break;
    case date < 3600 * 24 * 2:
      time = 'yesterday';
      break;
    default:
      time = `${Math.floor(date / (3600 * 24))} days ago`;
  }

  return time;
};
