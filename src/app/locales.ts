import { injectIntl as inject, InjectIntlConfig } from 'react-intl';
import { IS_BROWSER } from 'config';

const locales = [];

const localeData = {
  en: {},
  th: {},
};

locales.forEach((locale) => {
  localeData.en = { ...localeData.en, ...locale.en };
  localeData.th = { ...localeData.th, ...locale.th };
});

export const getLangWithoutCode = (lang) => lang.toLowerCase().split(/[_-]+/)[0];

export const getBrowserLanguage = () => {
  if (!IS_BROWSER) {
    return 'en';
  }
  const documentLang = document.documentElement.lang;
  const navigatorObj = navigator as any;
  const language = (
    (navigatorObj.languages && navigatorObj.languages[0]) ||
    navigatorObj.language || navigatorObj.userLanguage
  );

  if (documentLang) {
    return documentLang;
  } else {
    document.documentElement.lang = getLangWithoutCode(language);
  }

  return language;
};

export const getLocaleMessages = (lang?: string) => {
  const language = lang || getBrowserLanguage();
  // Split locales with a region code
  const languageWithoutRegionCode = getLangWithoutCode(language);
  return localeData[languageWithoutRegionCode] || localeData[language] || localeData.th;
};

export const injectIntl: any = (options: InjectIntlConfig) => (target: any) => {
  return inject(target, options);
};
