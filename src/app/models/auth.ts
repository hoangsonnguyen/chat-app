export interface IAuth {
  loading: boolean;
  error: boolean;
  errorMessage: string;
  jwt: string;
  user: any;
  loginSuccess: boolean;
  isLoggedIn: boolean;
  pendingVerify: boolean;
  isFirstLaunch: boolean;
}

export interface IAuthAction {
  type?: string;
  payload: any | { error: string; message: string; statusCode: number; };
  error: boolean;
}
