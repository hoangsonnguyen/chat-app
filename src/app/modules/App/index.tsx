import * as React from 'react';
import { push } from 'react-router-redux';
import { IState } from 'interfaces/components';
import { getUserInformation } from 'redux/reducers/auth';
import { CircularLoader } from 'components';
import { HandleMessageFirebase } from 'modules/HandleMessageFirebase';
import { toast, ToastContainer } from 'react-toastify';
import { IS_BROWSER } from 'config';

const { connect } = require('react-redux');
const style = require('./style.scss');

interface IProps {
  auth: any;
  changeRoute?(s: string): any;
  getUserInformation(): any;
}

const initialState = {
  settingUp: true,
  visibility: 'hidden',
};

@connect(
  (state) => ({ auth: state.auth }),
  (dispatch) => ({
    changeRoute: (s) => dispatch(push(s)),
    getUserInformation: () => dispatch(getUserInformation()),
  }),
)
class App extends React.Component<IProps, typeof initialState> {
  public state: IState<typeof initialState> = initialState;
  public componentDidMount() {
    const {getUserInformation, auth} = this.props;

    if (!auth.jwt) {
      return this.props.changeRoute('/auth');
    }

    getUserInformation().then((action) => {
      if (action.error) {
        return toast.error('something wrong');
      }

      return this.setState({ settingUp: false});
    });
    this.setState({visibility: 'visible'});
  }

  public render() {
    const { settingUp } = this.state;
    const { children } = this.props;
    if (settingUp) {
      return <div className={style.AppContainer_center}><CircularLoader size={32}/></div>;
    }

    if (!IS_BROWSER) {
      return null;
    }

    const {visibility} = this.state;

    return (
        <section className={style.AppContainer} style={{ visibility } as any}>
          <HandleMessageFirebase/>
          {children}
          <ToastContainer/>
        </section>
    );
  }
}

export { App }
