import * as React from 'react';
import { IState } from 'interfaces/components';
import { signupUser, loginUser } from 'redux/reducers/auth';
import { push } from 'react-router-redux';
import { toast, ToastContainer } from 'react-toastify';
const { connect } = require('react-redux');
const style = require('./style.scss');

interface IProps {
  auth: any;
  changeRoute?(s: string): any;
  loginUser(email: string, password: string): any;
  signupUser(email: string, password: string): any;

}

const initialState = {
  email: '',
  password: '',
  currentTab: 'signup',
  visibility: 'hidden',
};

@connect(
    (state) => ({ auth: state.auth }),
    (dispatch) => ({
      changeRoute: (s) => dispatch(push(s)),
      signupUser: (e, p) => dispatch(signupUser(e, p)),
      loginUser: (e, p) => dispatch(loginUser(e, p)),
    }),
  )
class Auth extends React.Component<Partial<IProps>, {}> {
  public state: IState<typeof initialState> = initialState;

  private onClick = () => {
    const {currentTab, email, password} = this.state;
    const {changeRoute, loginUser, signupUser} = this.props;

    if (!email || !password) {
      return toast.error('Try again please!');
    }

    if (currentTab === 'login') {
      return loginUser(email, password).then((action) => {
        if (action.error) {
          return toast.info(`Email or password doesn't seem right. Please try again`);
        }

        return changeRoute('/');
      });
    }

    return signupUser(email, password).then((action) => {
      if (action.error) {
        return toast.info(`Email is duplicated. Please try again`);
      }

      return changeRoute('/');
    });
  }

  public componentDidMount() {
    this.setState({visibility: 'visible'});
  }

  private onChangeTab = (tab) => {
    this.setState({currentTab: tab});
  }

  public render() {
    const {currentTab, visibility} = this.state;
    return (
      <div style={{ visibility } as any}>
        <div className={style.auth__title}>Realtime - Chat app</div>
        <div className={style.auth__tab}>
          <div>
            <span
                className={currentTab !== 'login' ? style.auth__active : null}
                onClick={() => this.onChangeTab('signup')}
            >
              Sign Up 
            </span>
            <span
                className={currentTab === 'login' ? style.auth__active : null}
                onClick={() => this.onChangeTab('login')}
              >
                Log In 
              </span>
          </div>
            
          <input
            className={style.auth__input}
            placeholder="enter your email"
            type="text"
            onChange={(e) => this.setState({email: e.target.value})}
          />
          <input
            className={style.auth__input}
            placeholder="enter your password"
            type="password"
            onChange={(e) => this.setState({password: e.target.value})}
          />
          <br />
          <button onClick={() => this.onClick()} className={style.auth__button}>
            {currentTab === 'login' ?  'login' : 'signup'}
          </button>
        </div>
        
        <ToastContainer/>
      </div>
    );
  }
}

export { Auth }
