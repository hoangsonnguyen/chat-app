import * as React from 'react';
import { getNotifications, updateNotification } from 'redux/reducers/notifications';
import { CircularLoader } from 'components';
import { toast } from 'react-toastify';
import { push } from 'react-router-redux';
import InfiniteScroll = require('react-infinite-scroller');
import { removeDeviceToken } from 'redux/reducers/auth';
import { getTime } from 'helpers/time';
const { connect } = require('react-redux');

const style = require('./style.scss');

interface IProps {
  auth: any;
  notifications: any;
  getNotifications(getNotifications: boolean): any;
  changeRoute(p: string): any;
  removeDeviceToken(): any;
  updateNotification(data: any, notificationId: string): any;
}

@connect(
  (state) => ({
    auth: state.auth,
    notifications: state.notifications,
  }),
  (dispatch) => ({
    getNotifications: (s) => dispatch(getNotifications(s)),
    removeDeviceToken: () => dispatch(removeDeviceToken()),
    changeRoute: (r: string) => dispatch(push(r)),
    updateNotification: (d, n) => dispatch(updateNotification(d, n)),
  }),
)
class Notifications extends React.Component<Partial<IProps>, {}> {
  private scrollParentRef = null;

  public componentDidMount() {
    this.props.getNotifications(false).then((action) => {
      if (action.error) {
        return toast.error('something wrong');
      }
    });
  }

  private updateNotification = (notification) => {
    if (notification.isRead) {
      return this.props.changeRoute(`/rooms/${notification.roomID}`);
    }

    this.props.updateNotification({isRead: true}, notification._id).then((action) => {
      if (action.error) {
        return toast.error('something wrong');
      }
      return this.props.changeRoute(`/rooms/${notification.roomID}`);
    });
  }

  private logout = () => {
    this.props.removeDeviceToken().then(() => {
      localStorage.clear();
      location.reload(true);
      return window.location.href = '/auth';
    });
  }

  public render() {
    const {
      notifications: {notifications, loading, hasMore},
      getNotifications, auth,
    } = this.props;

    const notificationNumber = notifications.filter((notification) => !notification.isRead).length;

    return (
      <div className={style.notification__wrapper}>
        <div className={style.notification__title}>
          <div className={style['notification__icon-wrapper']}>
            <img
              className={style.notification__icon}
              src="/public/assets/notification_icon.png"
            />
             {!!notificationNumber && (
                <div className={style.notification__number}>{notificationNumber}</div>
              )}
          </div>
          <button onClick={() => this.logout()}>
            {auth.loading ? 'Processing...' : 'Log out'}
          </button>
        </div>

        <div
          className={style.notification__scroll} style={{height: window && window.innerHeight - 70}}
          ref={(ref) => this.scrollParentRef = ref}
        >
          <InfiniteScroll
            hasMore={!loading && hasMore} initialLoad={false} threshold={200}
            loader={loading && <CircularLoader/>} pageStart={0}
            loadMore={() => hasMore && !loading && getNotifications(true)}
            useWindow={false}
            getScrollParent={() => this.scrollParentRef}
          >
            {loading && !notifications.length && (
              <div className={style.center}><CircularLoader size={32}/></div>
            )}
            {notifications.map((notification, i) => {
              const className = notification.isRead ? style.notification :
                `${style.notification} ${style['notification--active']}`;
              return (
                <div
                  className={className}
                  onClick={() => this.updateNotification(notification)}
                  key={i}
                >
                  {notification.type === 'ADDED_MESSAGE' && notification.title}
                  {notification.type === 'ADDED_ROOM' && notification.body}
                  <div className={style.notification__time}>{getTime(notification.createdAt)}</div>
                </div>
              );
            })}
          </InfiniteScroll>
        </div> 
      </div>
    );
  }
}

export {Notifications}
