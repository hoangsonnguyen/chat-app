import * as React from 'react';
import { getMessages, sendMessage } from 'redux/reducers/room';
import { getRoom, addUserToRoom } from 'redux/reducers/roomsList';
const { connect } = require('react-redux');
import { CircularLoader } from 'components';
import { IState } from 'interfaces/components';
import { WithRouterProps, withRouter } from 'react-router';
import InfiniteScroll = require('react-infinite-scroller');
import { localizedDate } from 'helpers/localizedDate';
import {reverseMap} from 'helpers/loop';
import { toast } from 'react-toastify';

const style = require('./style.scss');

interface IProps extends Partial<WithRouterProps> {
  auth: any;
  room: any;
  roomsList: any;
  addUserToRoom(data: any, roomId: string): any;
  getRoom(roomId: string): any;
  addTemporaryMessageToRoom(data: any): any;
  getMessages(roomId: string, shouldGetMore: boolean): any;
  sendMessage(data: any, roomId: string, user: any, resend: boolean): any;
}

const initialState = {
  showMessageError: false,
  newMessage: '',
};
export const withRouterDecorator = () => (component) => withRouter(component);

@withRouterDecorator()
@connect(
  (state) => ({
    room: state.room,
    auth: state.auth,
    roomsList: state.roomsList,
  }),
  (dispatch) => ({
    getRoom: (r) => dispatch(getRoom(r)),
    getMessages: (r, s) => dispatch(getMessages(r, s)),
    sendMessage: (d, r, u, s) => dispatch(sendMessage(d, r, u, s)),
    addUserToRoom: (d, r) => dispatch(addUserToRoom(d, r)),
  }),
)
class Room extends React.Component<Partial<IProps>, any> {
  public state: IState<typeof initialState> = initialState;
  private scrollParentRef = null;

  public componentDidMount() {
    const {params: {roomId}, addUserToRoom, auth: {user}} = this.props;

    if (!roomId) {
      return;
    }

    this.props.getRoom(roomId).then((action) => {
      if (!action.error) {
        return this.props.getMessages(roomId, false).then((action) => {
          if (!action.payload.list.length) {
            return;
          }
          return this.gotoBottom();
        });
      }
      addUserToRoom({users: [user._id]}, roomId).then((action) => {
        if (action.error) {
          return toast.error('something wrong');
        }
        return this.props.getRoom(roomId).then(() => {
          this.props.getMessages(roomId, false).then((action) => {
            if (!action.payload.list.length) {
              return;
            }
            return this.gotoBottom();
          });
        });
      });
    });

  }

  public componentWillReceiveProps(nextProps) {
    const { params: {roomId: prevRoomId}} = this.props;
    const { params: {roomId: nextRoomId}} = nextProps;
    const isRoomIdChanged = prevRoomId !== nextRoomId;

    if (!isRoomIdChanged) {
      return;
    }

    this.props.getRoom(nextRoomId);
    return this.props.getMessages(nextRoomId, false).then((action) => {
      if (!action.payload.list.length) {
        return;
      }
      return this.gotoBottom();
    });
  }

  private sendMessage = (message, resend) => {
    const {params: {roomId}, sendMessage, auth: {user}} = this.props;
    const newMessage = message;

    document.getElementById('contentEditable').textContent = '';
    this.gotoBottom();

    sendMessage(newMessage, roomId, user, resend).then((action) => {
      if (action.error) {
        return this.setState({showMessageError: true});
      }
      return this.setState({showMessageError: false});
    });
  }

  private onKeyPress = (e) => {
    const newMessage = e.target.textContent;
    this.setState({newMessage});

    if (e.charCode !== 13) {
      return;
    }

    this.sendMessage(newMessage, false);
    e.preventDefault();
  }

  private gotoBottom = () => {
    const roomElement = document.getElementById('room');
    if (!roomElement) {
      return;
    }
    const scrollElement = document.getElementById('room').firstElementChild;
    if (!scrollElement) {
      return;
    }
    roomElement.scrollTop = scrollElement.scrollHeight - roomElement.clientHeight + 10;
  }

  public render() {
    const {showMessageError, newMessage} = this.state;
    const {
      room: {room, loading, hasMore}, auth: {user}, params: {roomId},
      roomsList: {currentRoom, waitingToAddMember, waitingToCreateRoom}, getMessages,
    } = this.props;
    let prevUserId = '';
    const height = window && window.innerHeight - 128;

    return (
      <div style={{height}}>
        <div className={style.message__bar}>
          <img src="/public/assets/group.svg"/> 
          <div className={style['message__username-list-wrapper']}>
            {currentRoom.members && currentRoom.members.map((member, i) => {
              let email = member.email;
              if (member._id === user._id) {
                email = 'me';
              }

              return (
                <span className={style['message__username-list']} key={i}>{email}</span>
              );
            })}
          </div>
          
        </div>
        <div
          className={style.message__wrapper}
          id="room"
          ref={(ref) => this.scrollParentRef = ref}
        >
            <InfiniteScroll
              hasMore={!loading && hasMore} initialLoad={false} threshold={100}
              loader={loading && <CircularLoader />} pageStart={0}
              loadMore={() => hasMore && !loading && getMessages(roomId, true)}
              useWindow={false}
              isReverse={true}
              getScrollParent={() => this.scrollParentRef}
            >
              {waitingToAddMember && <div className={style.center}>Waiting to join...</div>}
              {waitingToCreateRoom && <div className={style.center}>Waiting to create room...</div>}
              {!roomId && !waitingToAddMember && !waitingToCreateRoom && (
              <div className={style.center}>
                Please select a room in the left side to start a conversation!
              </div>)}
              {loading && !hasMore && (
                <div className={style.center}><CircularLoader size={32}/></div>
              )}
              {reverseMap(room, ((message, i) => {
                if (!message) {
                  return null;
                }

                const isSameUser = prevUserId === message.createdBy._id;
                const isCurrentUser = message.createdBy._id === user._id;

                prevUserId = message.createdBy._id;

                return (
                <div key={i} >
                  {!isSameUser && (
                    <div className={style.message__time}>{localizedDate(message.createdAt)}</div>
                  )}
                  {!isSameUser && !isCurrentUser && (
                    <div className={style.message__user}>
                      <img src="/public/assets/user.png"/>
                      <div className={style.message__username}>{message.createdBy.email}</div>
                    </div>
                  )}
                  <div className={style.message__clearfix}>
                  <div className={isCurrentUser ? style.right : style.left}>
                    <div className={style.message__content}>{message.content}</div>
                  </div>
                  </div>
                 
                </div>
                );
              }))}
              {showMessageError && (
                <div
                  className={style.message__error}
                  onClick={() => this.sendMessage(newMessage, true)}
                >
                  This message didn't send. Check your internet connection and click to try again
                </div>
              )}
              <div className={style.message__bottom}/>
            </InfiniteScroll>
          </div>
          {roomId && (
            <div className={style.message__background}>
            <div className={style.message__input}>
              <div
                id="contentEditable"
                className={style.contentEditable}
                contentEditable={true}
                onKeyPress={(e) => this.onKeyPress(e)}
                data-placeholder="type a message..."
              />
            </div>
          </div>
        )}
      </div>
    );
  }
}

export {Room}
