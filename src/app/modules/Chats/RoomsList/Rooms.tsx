import * as React from 'react';
import { getRooms, addUserToRoom } from 'redux/reducers/roomsList';
import { CircularLoader } from 'components';
import { toast } from 'react-toastify';
import { WithRouterProps, withRouter } from 'react-router';
import { push } from 'react-router-redux';
import InfiniteScroll = require('react-infinite-scroller');
import { getTime } from 'helpers/time';
import { getRoomName } from 'helpers/getRoomName';
const { connect } = require('react-redux');

const style = require('./style.scss');

interface IProps extends Partial<WithRouterProps> {
  roomsList: any;
  user: any;
  getRooms(shouldGetMore: boolean): any;
  changeRoute(p: string): any;
  addUserToRoom(data: any, roomId: string): any;
}

export const withRouterDecorator = () => (component) => withRouter(component);

@withRouterDecorator()
@connect(
  (state) => ({
    user: state.auth.user,
    roomsList: state.roomsList,
  }),
  (dispatch) => ({
    changeRoute: (r: string) => dispatch(push(r)),
    getRooms: (s) => dispatch(getRooms(s)),
    addUserToRoom: (d, r) => dispatch(addUserToRoom(d, r)),
  }),
)
class Rooms extends React.Component<Partial<IProps>, {}> {
  private scrollParentRef = null;

  public componentDidMount() {
    this.props.getRooms(false).then((action) => {
      if (action.error) {
        return toast.error('something wrong');
      }
    });
  }

  private addUserToRoom = (room) => {
    const {user, addUserToRoom, changeRoute} = this.props;
    for (const item of room.members) {
      if (item._id === user._id) {
        return changeRoute(`/rooms/${room._id}`);
      }
    }

    addUserToRoom({users: [user._id]}, room._id).then((action) => {
      if (action.error) {
        return toast.error('something wrong');
      }
      return changeRoute(`/rooms/${room._id}`);
    });
  }

  public render() {
    const {
      roomsList: {roomsList, loading, hasMore},
      getRooms, params: {roomId}, user,
    } = this.props;

    if (loading && !roomsList.length) {
      return <CircularLoader size={32}/>;
    }

    return (
      <div
        className={style.room__wrapper} style={{height: window && window.innerHeight - 120}}
        ref={(ref) => this.scrollParentRef = ref}
      >
        <InfiniteScroll
          hasMore={!loading && hasMore} initialLoad={false} threshold={200}
          loader={loading && <CircularLoader/>} pageStart={0}
          loadMore={() => hasMore && !loading && getRooms(true)}
          useWindow={false}
          getScrollParent={() => this.scrollParentRef}
        >
          {roomsList.map((room) => {
            const isActive = roomId === room._id;
            const hasNewMessage = room.lastMessage && room.lastMessage.isNew;
            return (
              <div
                className={isActive ? `${style.room} ${style['room__room--active']}` : style.room}
                key={room._id}
                onClick={() => this.addUserToRoom(room)}
              >
                <img src="/public/assets/user.png"/>
                <div>
                  <div className={hasNewMessage ? `${style.room_username} ${style['room__lastmessage-active']}`
                   : style.room_username}
                  >
                    {getRoomName(room.members, user._id)}
                  </div>

                  {room.lastMessage && (
                    <div className={style.room_message}>
                      <span className={hasNewMessage ? `${style.room_content} ${style['room__lastmessage-active']}`
                        : style.room_content}>{room.lastMessage.content}</span>
                      <span>{getTime(room.lastMessage.createdAt)}</span>
                    </div>
                  )}

                  {!room.lastMessage && (
                    <div className={style.room_message}>New message to</div>
                  )}
                </div>
              </div>
            );
          })}
        </InfiniteScroll>
      </div>
    );
  }
}

export {Rooms}
