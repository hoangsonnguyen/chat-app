import * as React from 'react';
import { CircularLoader } from 'components';
import InfiniteScroll = require('react-infinite-scroller');
import { IState } from 'interfaces/components';
import { searchUsers } from 'redux/reducers/users';
import { createRoom } from 'redux/reducers/roomsList';
import { push } from 'react-router-redux';
import { toast } from 'react-toastify';
const { connect } = require('react-redux');

const style = require('./style.scss');

interface IProps {
  emailName: any;
  users: any;
  auth: any;
  changeRoute(p: string): any;
  searchUsers(emailName: string, shouldGetMore: boolean): any;
  createRoom(data: any, userInRoom): any;
}

const initialState = {
  emailName: '',
};

@connect(
  (state) => ({
    auth: state.auth,
    users: state.users,
  }),
  (dispatch) => ({
    changeRoute: (r: string) => dispatch(push(r)),
    searchUsers: (e, s) => dispatch(searchUsers(e, s)),
    createRoom: (d, u) => dispatch(createRoom(d, u)),
  }),
)
class UsersList extends React.Component<Partial<IProps>, {}> {
  public state: IState<typeof initialState> = initialState;
  private scrollParentRef = null;

  private createRoom = (data, userInRoom) => {
    const {changeRoute, createRoom} = this.props;
    createRoom(data, userInRoom).then((action) => {
      if (action.error) {
        return toast.error('something wrong');
      }
      return changeRoute(`/rooms/${action.payload._id}`);
    });
  }

  public render() {
    const {
        users: {users, loading, hasMore},
        searchUsers, emailName, auth,
    } = this.props;

    if (loading && !users.length) {
      return <CircularLoader size={32}/>;
    }

    return (
      <div
        className={style.room__wrapper} style={{height: window && window.innerHeight - 120}}
        ref={(ref) => this.scrollParentRef = ref}
        id="users_list"
      >
        {!!users.length && (
          <InfiniteScroll
            hasMore={!loading && hasMore} initialLoad={false} threshold={200}
            loader={loading && <CircularLoader/>} pageStart={0}
            loadMore={() => hasMore && !loading && searchUsers(emailName, true)}
            useWindow={false}
            getScrollParent={() => this.scrollParentRef}
          >
            <div className={style.room__contact}>Contacts</div>
            {users.map((user, i) => {
              const data = {
                name: 'new room',
                users: [auth.user._id, user._id],
              };

              const userInRoom = [
                {_id: auth.user._id, email: auth.user.email},
                {_id: user._id, email: user.email},
              ];

              return (
                <div key={i} className={style.room__users}>
                  {user.email} 
                  <span className={style.room__icon} onMouseDown={() => this.createRoom(data, userInRoom)}>+</span>
                </div>
              );
            })}
          </InfiniteScroll>
        )}
        {!users.length && <div className={style.room__notfound}>Nothing found</div>}  
      </div>
    );
  }
}

export {UsersList}
