import * as React from 'react';
import { toast } from 'react-toastify';
import { WithRouterProps, withRouter } from 'react-router';
import { IState } from 'interfaces/components';
import { searchUsers, resetUsers } from 'redux/reducers/users';
import { UsersList } from './UsersList';
import { Rooms } from './Rooms';
import { push } from 'react-router-redux';

const { connect } = require('react-redux');

const style = require('./style.scss');

interface IProps extends Partial<WithRouterProps> {
  auth: any;
  roomsList: any;
  searchUsers(emailName: string, shouldGetMore: boolean): any;
  resetUsers(): any;
}

const initialState = {
  emailName: '',
  showUsers: false,
};

export const withRouterDecorator = () => (component) => withRouter(component);

@withRouterDecorator()
@connect(
  (state) => ({
    auth: state.auth,
    roomsList: state.roomsList,
  }),
  (dispatch) => ({
    changeRoute: (r: string) => dispatch(push(r)),
    searchUsers: (e, s) => dispatch(searchUsers(e, s)),
    resetUsers: () => dispatch(resetUsers()),
  }),
)
class RoomsList extends React.Component<Partial<IProps>, {}> {
  public state: IState<typeof initialState> = initialState;

  private searchUsers = (e) => {
    const emailName = e.target.value;
    this.props.searchUsers(emailName, false).then((action) => {
      if (action.error) {
        return toast.error('something wrong');
      }
    });
    this.setState({emailName});
  }

  private onBlur = () => {
    this.props.resetUsers();
    this.setState({showUsers: false, emailName: ''});
  }

  public render() {
    const {emailName, showUsers} = this.state;
    const {
      roomsList: {waitingToCreateRoom},
      auth,
    } = this.props;

    return (
      <div className={style.rooms__wrapper}>
        <div className={style.rooms_username}>
          <h4>{auth.user.email}</h4>
        </div>
      
        <input
          className={style.rooms__input}
          onChange={(e) => this.searchUsers(e)}
          placeholder="Create a room"
          onFocus={() => this.setState({showUsers: true})}
          onBlur={() => this.onBlur()}
          value={emailName}
          />
        {!showUsers && <Rooms/>}
        {showUsers && !waitingToCreateRoom && (
          <UsersList emailName={emailName} />
        )}
      </div>
    );
  }
}

export {RoomsList}
