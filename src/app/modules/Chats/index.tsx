import * as React from 'react';
import { RoomsList } from './RoomsList';
import { Room } from './Room';
import { Notifications } from './Notifications';
const style = require('./style.scss');

class Chats extends React.Component<{}, {}> {
  public render() {
    return (
      <div className={style.chat__container}>
        <RoomsList/>
        <Room/>
        <Notifications/>
      </div>
    );
  }
}

export {Chats}
