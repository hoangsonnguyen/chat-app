import * as React from 'react';
import * as firebase from 'firebase/app';
import 'firebase/messaging';
import { WithRouterProps, withRouter } from 'react-router';
import { integrateNotifications } from '../../redux/reducers/integrations';
import { receiveMessage } from 'redux/reducers/room';
import { addNotificationFromFireBase } from 'redux/reducers/notifications';
import { toast } from 'react-toastify';
import { updateCurrentRoom } from 'redux/reducers/roomsList';
const { connect } = require('react-redux');

interface IProps extends Partial<WithRouterProps> {
  auth: any;
  roomsList: any;
  integrateNotifications?(t: string): Promise<any>;
  receiveMessage(data: any): any;
  updateCurrentRoom(data: any): any;
  addNotificationFromFireBase(notification: any): any;
}
export const withRouterDecorator = () => (component) => withRouter(component);
@withRouterDecorator()
@connect(
  (state) => ({
    auth: state.auth,
    roomsList: state.roomsList,
  }),
  (dispatch) => ({
    integrateNotifications: (t) => dispatch(integrateNotifications(t)),
    receiveMessage: (d) => dispatch(receiveMessage(d)),
    updateCurrentRoom: (d) => dispatch(updateCurrentRoom(d)),
    addNotificationFromFireBase: (n) => dispatch(addNotificationFromFireBase(n)),
  }),
)
class HandleMessageFirebase extends React.Component<Partial<IProps>, {}> {

  public componentDidMount() {
    navigator.serviceWorker.addEventListener('message', (event) => {
      console.log('event listener', event);
      if (event.data['firebase-messaging-msg-data']) {
        return;
      }
      this.handleNotificationReceived(event.data, true);
    });

    this.setupFirebase();
    setTimeout(() => {
      this.requestNotificationPermission();
    }, 2000);
  }

  private setupFirebase() {
    firebase.initializeApp({
      messagingSenderId: '943829742668',
    });
  }

  private requestNotificationPermission() {
    const messaging = firebase.messaging();
    messaging
      .requestPermission()
      .then(() => {
        setTimeout(() => {
          firebase
            .messaging()
            .getToken()
            .then(this.handleTokenReceived)
            .catch(console.log);
        }, 2000);
      }).catch((err) => {
        console.log('Unable to get permission to notify.', err);
      });

    messaging.onMessage(this.handleNotificationReceived);
  }

  private handleTokenReceived = (token: string) => {
    // send to out server for integration
    if (token) {
      localStorage.setItem('deviceToken', JSON.stringify(token));
      this.props.integrateNotifications(token);
    }
    console.log(token);
  }

  private handleNotificationReceived = (data, unfocus?) => {
    console.log(data);
    const {data: message, notification} = data;
    const {
      receiveMessage, auth, updateCurrentRoom,
      params: {roomId}, addNotificationFromFireBase,
    } = this.props;
    const isCurrentRoom = roomId === message.roomID;

    if (!message) {
      return;
    }

    const {
      content, createdBy, createdAt, email,
      roomID, event, notificationID, targetUID,
    } = message;
    const isSameUser = auth.user._id === createdBy;

    if (targetUID && targetUID !== auth.user._id) {
      return;
    }

    if (isSameUser || (!notification && !unfocus)) {
      return;
    }

    if (event === 'ADDED_MESSAGE') {
      receiveMessage({
        ...message,
        content: data.notification ? data.notification.body : content,
        roomID,
        createdAt,
        isCurrentRoom,
        createdBy: {
          _id: createdBy,
          createdAt,
          email,
        },
      });
    }

    if (!unfocus) {
      addNotificationFromFireBase({
        ...notification,
        title: `New message from ${email}`,
        isRead: false,
        _id: notificationID,
        roomID,
        createdBy,
        createdAt: new Date(),
        type: event,
      });
    }

    if (event === 'ADDED_ROOM') {
      updateCurrentRoom({
        email,
        isCurrentRoom,
      });
    }

    if (!unfocus && event === 'ADDED_MESSAGE') {
      toast.success(`New message from ${email}`);
    }

    if (!unfocus && event === 'ADDED_ROOM') {
      toast.success(notification.body);
    }
  }

  public render() {
    return null;
  }

}

export { HandleMessageFirebase }
