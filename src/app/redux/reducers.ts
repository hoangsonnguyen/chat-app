import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { roomsListReducer } from './reducers/roomsList';
import { roomReducer } from './reducers/room';
import { authReducer } from './reducers/auth';
import { notificationReducer } from './reducers/notifications';
import { usersListReducer } from './reducers/users';

const { reducer } = require('redux-connect');

const rootReducer: Redux.Reducer<any> = combineReducers<any>({
  routing: routerReducer,
  reduxAsyncConnect: reducer,
  roomsList: roomsListReducer,
  users: usersListReducer,
  room: roomReducer,
  auth: authReducer,
  notifications: notificationReducer,
});

export default rootReducer;
