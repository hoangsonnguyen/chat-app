import { CALL_API } from 'redux-api-middleware';
import { IS_BROWSER } from '../../../config';

/** Action Types */
export const LOGIN_USER_REQUEST: string = 'auth/LOGIN_USER_REQUEST';
export const LOGIN_USER_SUCCESS: string = 'auth/LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE: string = 'auth/LOGIN_USER_FAILURE';

export const SIGNUP_USER_REQUEST: string = 'auth/SIGNUP_USER_REQUEST';
export const SIGNUP_USER_SUCCESS: string = 'auth/SIGNUP_USER_SUCCESS';
export const SIGNUP_USER_FAILURE: string = 'auth/SIGNUP_USER_FAILURE';

export const GET_USER_INFO_REQUEST: string = 'auth/GET_USER_INFO_REQUEST';
export const GET_USER_INFO_SUCCESS: string = 'auth/GET_USER_INFO_SUCCESS';
export const GET_USER_INFO_FAILURE: string = 'auth/GET_USER_INFO_FAILURE';

export const REMOVE_TOKEN_REQUEST: string = 'auth/REMOVE_TOKEN_REQUEST';
export const REMOVE_TOKEN_SUCCESS: string = 'auth/REMOVE_TOKEN_SUCCESS';
export const REMOVE_TOKEN_FAILURE: string = 'auth/REMOVE_TOKEN_FAILURE';

export const LOGOUT_USER: string = 'auth/LOGOUT_USER';

export const HYDRATE_STATE: string = 'auth/HYDRATE_STATE';
export const SET_USER_DATA: string = 'auth/SET_USER_DATA';

export const REDUX_INIT: string = '@@redux/INIT';
/** Auth: Initial State */
const initialState = {
  loading: false,
  jwt: null,
  user: null,
};

// ⚠ WARNING: This exact function is being used in HTML/index.tsx file to serialize the state
// If any changes are made here, please conisder changing there too
const getInitialState = () => {
  if (!IS_BROWSER) {
    return initialState;
  }

  const authData: {jwt: string} = JSON.parse(localStorage.getItem('@auth'));
  return {
    ...initialState,
    jwt: authData && authData.jwt,
  };
};

export function authReducer(state = getInitialState(), action?: any) {
  switch (action.type) {
    case LOGIN_USER_REQUEST:
    case SIGNUP_USER_REQUEST:
    case GET_USER_INFO_REQUEST:
    case REMOVE_TOKEN_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case GET_USER_INFO_SUCCESS:
      return {
        ...state,
        loading: false,
        user: action.payload,
      };
    case LOGIN_USER_SUCCESS:
    case SIGNUP_USER_SUCCESS:
      const accessToken = {jwt: action.payload.accessToken};
      localStorage.setItem('@auth', JSON.stringify(accessToken));
      return {
        ...state,
        ...accessToken,
        loading: false,
      };

    case SET_USER_DATA:
      return {
        ...state,
        user: {
          ...action.payload.data,
        },
      };

    case REMOVE_TOKEN_SUCCESS:
    case SIGNUP_USER_FAILURE:
    case LOGIN_USER_FAILURE:
    case GET_USER_INFO_FAILURE:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}

export function loginUser(email: string, password: string) {
  return ({
    [CALL_API]: {
      method: 'POST',
      headers: {
        'NO-AUTH': true,
      },
      endpoint: '/auth/login',
      body: JSON.stringify({
        email, password,
      }),
      types: [
        LOGIN_USER_REQUEST,
        LOGIN_USER_SUCCESS,
        LOGIN_USER_FAILURE,
      ],
    },
  });
}

export function signupUser(email: string, password: string) {
  return ({
    [CALL_API]: {
      method: 'POST',
      headers: {
        'NO-AUTH': true,
      },
      endpoint: '/auth/signup',
      body: JSON.stringify({
        email, password,
      }),
      types: [
        SIGNUP_USER_REQUEST,
        SIGNUP_USER_SUCCESS,
        SIGNUP_USER_FAILURE,
      ],
    },
  });
}

export function authUserData(user) {
  return { type: SET_USER_DATA, payload: { data: user } };
}

export function getUserInformation() {
  return ({
    [CALL_API]: {
      method: 'GET',
      endpoint: `/users/me`,
      types: [
        GET_USER_INFO_REQUEST,
        GET_USER_INFO_SUCCESS,
        GET_USER_INFO_FAILURE,
      ],
    },
  });
}

export function removeDeviceToken() {
  const token = JSON.parse(localStorage.getItem('deviceToken'));

  return ({
    [CALL_API]: {
      method: 'PUT',
      endpoint: `/users/me/token/remove`,
      body: JSON.stringify({token}),
      types: [
        REMOVE_TOKEN_REQUEST,
        REMOVE_TOKEN_SUCCESS,
        REMOVE_TOKEN_FAILURE,
      ],
    },
  });
}
