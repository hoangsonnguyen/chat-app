import { CALL_API } from 'redux-api-middleware';

/** Action Types */
export const POST_INTEGRATION_NOTIFICATIONS_SUCCESS: string = 'integrations/POST_INTEGRATION_NOTIFICATIONS_SUCCESS';
export const POST_INTEGRATION_NOTIFICATIONS_FAILURE: string = 'integrations/POST_INTEGRATION_NOTIFICATIONS_FAILURE';
export const POST_INTEGRATION_NOTIFICATIONS_REQUEST: string = 'integrations/POST_INTEGRATION_NOTIFICATIONS_REQUEST';

/** integrations: Initial State */
const initialState = {
  loading: false,
  error: false,
  errorMessage: '',
};

/** Reducer: IntegrationsReducer */
export function integrationsReducer(state = initialState, action?) {
  switch (action.type) {
    case POST_INTEGRATION_NOTIFICATIONS_REQUEST:
      return {
        ...state,
        loading: true,
        error: false,
        errorMessage: '',
      };
    case POST_INTEGRATION_NOTIFICATIONS_SUCCESS:
      return {
        ...state,
        error: false,
        loading: false,
      };
    case POST_INTEGRATION_NOTIFICATIONS_FAILURE:
      return {
        ...state,
        loading: false,
        error: true,
        errorMessage: action.payload.message,
      };
    default:
      return state;
  }
}

export function integrateNotifications(token: string) {
  return ({
    [CALL_API]: {
      method: 'PUT',
      endpoint: `/users/me/token/add`,
      body: JSON.stringify({token}),
      types: [
        POST_INTEGRATION_NOTIFICATIONS_REQUEST,
        POST_INTEGRATION_NOTIFICATIONS_SUCCESS,
        POST_INTEGRATION_NOTIFICATIONS_FAILURE,
      ],
    },
  });
}
