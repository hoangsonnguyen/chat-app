import { CALL_API, getJSON } from 'redux-api-middleware';

export const GET_NOTIFICATION_REQUEST: string = 'notification/GET_NOTIFICATION_REQUEST';
export const GET_NOTIFICATION_SUCCESS: string = 'notification/GET_NOTIFICATION_SUCCESS';
export const GET_NOTIFICATION_FAILURE: string = 'notification/GET_NOTIFICATION_FAILURE';

export const UPDATE_NOTIFICATION_REQUEST: string = 'notification/UPDATE_NOTIFICATION_REQUEST';
export const UPDATE_NOTIFICATION_SUCCESS: string = 'notification/UPDATE_NOTIFICATION_SUCCESS';
export const UPDATE_NOTIFICATION_FAILURE: string = 'notification/UPDATE_NOTIFICATION_FAILURE';

export const ADD_NOTIFICATION_SUCCESS: string = 'notification/ADD_NOTIFICATION_SUCCESS';

const initialState = {
  notifications: [],
  cursor: '',
  hasMore: false,
  loading: false,
};

export function notificationReducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_NOTIFICATION_REQUEST:
      return {
        ...state,
      };
    case GET_NOTIFICATION_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ADD_NOTIFICATION_SUCCESS:
      return {
        ...state,
        notifications: [action.payload].concat(state.notifications),
      };
    case UPDATE_NOTIFICATION_SUCCESS:
      let notifications = [...state.notifications];
      notifications = notifications.map((notification) => {
        if (notification._id === action.payload) {
          return {
            ...notification,
            isRead: true,
          };
        }
        return notification;
      });
      return {
        ...state,
        notifications,
      };
    case GET_NOTIFICATION_SUCCESS:
      const {list, totalCount, cursor, shouldGetMore, userId} = action.payload;
      const filterNotification = list.filter((notification) => notification.createdBy !== userId);
      let newNotifications = [];
      let hasMore = false;

      if (!shouldGetMore) {
        newNotifications = filterNotification;
        hasMore = list.length < totalCount;
      }

      if (shouldGetMore) {
        newNotifications = state.notifications.concat(filterNotification);
        hasMore = state.notifications.concat(list).length < totalCount;
      }

      return {
        notifications: newNotifications,
        loading: false,
        cursor,
        hasMore,
      };
    case GET_NOTIFICATION_FAILURE:
    case UPDATE_NOTIFICATION_FAILURE:
      return {
        ...state,
        loading: true,
      };
    default:
      return state;
  }
}

export function getNotifications(shouldGetMore: boolean) {
  return (dispatch, getState) => {
    const {notifications: {cursor: prevCursor}, auth: {user}} = getState();
    let endpoint = `/notifications?limit=20&sortDirection=DESC&sortBy=createdAt`;

    if (shouldGetMore) {
      endpoint += `&cursor=${prevCursor}`;
    }

    return dispatch({
      [CALL_API]: {
        method: 'GET',
        endpoint,
        types: [
          GET_NOTIFICATION_REQUEST,
          {
            type: GET_NOTIFICATION_SUCCESS,
            payload: (_, __, res) => {
              return getJSON(res).then((json) => {
                return {
                  ...json,
                  userId: user._id,
                  shouldGetMore,
                };
              });
            },
          },
          GET_NOTIFICATION_FAILURE,
        ],
      },
    });
  };
}

export function updateNotification(data, notificationID) {
  return {
    [CALL_API]: {
      method: 'PUT',
      endpoint: `/notifications/${notificationID}`,
      body: JSON.stringify(data),
      types: [
        UPDATE_NOTIFICATION_REQUEST,
        {
          type: UPDATE_NOTIFICATION_SUCCESS,
          payload: notificationID,
        },
        UPDATE_NOTIFICATION_FAILURE,
      ],
    },
  };
}

export function addNotificationFromFireBase(data) {
  return {
    type: ADD_NOTIFICATION_SUCCESS,
    payload: data,
  };
}
