import { CALL_API, getJSON } from 'redux-api-middleware';

export const GET_MESSAGES_REQUEST: string = 'room/GET_MESSAGES_REQUEST';
export const GET_MESSAGES_SUCCESS: string = 'room/GET_MESSAGES_SUCCESS';
export const GET_MESSAGES_FAILURE: string = 'room/GET_MESSAGES_FAILURE';

export const SEND_MESSAGE_REQUEST: string = 'room/SEND_MESSAGE_REQUEST';
export const SEND_MESSAGE_SUCCESS: string = 'room/SEND_MESSAGE_SUCCESS';
export const SEND_MESSAGE_FAILURE: string = 'room/SEND_MESSAGE_FAILURE';

export const RECEIVE_MESSAGE_SUCCESS: string = 'room/RECEIVE_MESSAGE_SUCCESS';

export const ADD_TEMPORARY_MESSAGE_SUCCESS: string = 'room/ADD_TEMPORARY_MESSAGE_SUCCESS';

export const RESET_ROOM_PAGINATION: string = 'room/RESET_ROOM_PAGINATION';

const initialState = {
  room: [],
  cursor: '',
  hasMore: false,
  loading: false,
};

export function roomReducer(state = initialState, action) {
  const room = [...state.room];

  switch (action.type) {
    case GET_MESSAGES_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case RESET_ROOM_PAGINATION:
      return {
        room: [],
        cursor: '',
        hasMore: false,
        loading: false,
      };
    case SEND_MESSAGE_REQUEST:
      const {newMessage, user, resend} = action.payload;

      if (resend) {
        return state;
      }

      const temporaryMessage = {
        content: newMessage,
        createdAt: new Date(),
        createdBy: {
          _id: user._id,
          email: user.email,
        },
        _id: new Date().getTime(),
      };
      return {
        ...state,
        loading: false,
        room: [temporaryMessage].concat(room),
      };
    case RECEIVE_MESSAGE_SUCCESS:
      if (!action.payload.isCurrentRoom) {
        return state;
      }

      return {
        ...state,
        loading: false,
        room: [action.payload].concat(room),
      };
    case SEND_MESSAGE_SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case GET_MESSAGES_SUCCESS:
      const {list, totalCount, cursor, shouldGetMore} = action.payload;
      let newRoom = [];

      if (!shouldGetMore) {
        newRoom = list;
      }
      if (shouldGetMore) {
        newRoom = state.room.concat(list);
      }

      return {
        room: newRoom,
        cursor,
        hasMore: newRoom.length < totalCount,
        loading: false,
      };
    case GET_MESSAGES_FAILURE:
    case SEND_MESSAGE_FAILURE:
      return {
        ...state,
        loading: true,
      };
    default:
      return state;
  }
}

export function getMessages(roomId: string, shouldGetMore: boolean)  {
  return (dispatch, getState) => {
    const {room: {cursor: prevCursor}} = getState();

    let endpoint = `/rooms/${roomId}/messages?limit=20&sortDirection=DESC&sortBy=createdAt`;

    if (shouldGetMore) {
      endpoint += `&cursor=${prevCursor}`;
    }

    return dispatch({
      [CALL_API]: {
        method: 'GET',
        endpoint,
        types: [
          GET_MESSAGES_REQUEST,
          {
            type: GET_MESSAGES_SUCCESS,
            payload: (_, __, res) => {
              return getJSON(res).then((json) => {
                return {
                  ...json,
                  shouldGetMore,
                };
              });
            },
          },
          GET_MESSAGES_FAILURE,
        ],
      },
    });
  };
}

export function sendMessage(newMessage: any, roomId: string, currentUser: any, resend: boolean) {
  return {
    [CALL_API]: {
      method: 'POST',
      endpoint: `/rooms/${roomId}/messages`,
      body: JSON.stringify({content: newMessage}),
      types: [
        {
          type: SEND_MESSAGE_REQUEST,
          payload: {
            newMessage,
            user: currentUser,
            resend,
          },
        },
        SEND_MESSAGE_SUCCESS,
        SEND_MESSAGE_FAILURE,
      ],
    },
  };
}

export function receiveMessage(data) {
  return {
    type: RECEIVE_MESSAGE_SUCCESS,
    payload: data,
  };
}
