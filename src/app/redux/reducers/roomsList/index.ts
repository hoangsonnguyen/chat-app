import { CALL_API, getJSON } from 'redux-api-middleware';
import { SEND_MESSAGE_SUCCESS, RECEIVE_MESSAGE_SUCCESS } from '../room';

export const GET_ROOMS_REQUEST: string = 'roomsList/GET_ROOMS_REQUEST';
export const GET_ROOMS_SUCCESS: string = 'roomsList/GET_ROOMS_SUCCESS';
export const GET_ROOMS_FAILURE: string = 'roomsList/GET_ROOMS_FAILURE';

export const GET_ROOM_REQUEST: string = 'roomsList/GET_ROOM_REQUEST';
export const GET_ROOM_SUCCESS: string = 'roomsList/GET_ROOM_SUCCESS';
export const GET_ROOM_FAILURE: string = 'roomsList/GET_ROOM_FAILURE';

export const ADD_USER_TO_ROOM_REQUEST: string = 'roomsList/ADD_USER_TO_ROOM_REQUEST';
export const ADD_USER_TO_ROOM_SUCCESS: string = 'roomsList/ADD_USER_TO_ROOM_SUCCESS';
export const ADD_USER_TO_ROOM_FAILURE: string = 'roomsList/ADD_USER_TO_ROOM_FAILURE';

export const CREATE_ROOM_REQUEST: string = 'roomsList/CREATE_ROOM_REQUEST';
export const CREATE_ROOM_SUCCESS: string = 'roomsList/CREATE_ROOM_SUCCESS';
export const CREATE_ROOM_FAILURE: string = 'roomsList/CREATE_ROOM_FAILURE';

export const UPDATE_CURRENT_ROOM_SUCCESS: string = 'roomsList/UPDATE_CURRENT_ROOM_SUCCESS';

const initialState = {
  roomsList: [],
  currentRoom: {} as any,
  cursor: '',
  hasMore: false,
  loading: false,
  waitingToAddMember: false,
  waitingToCreateRoom: false,
};

export function roomsListReducer(state = initialState, action) {
  const roomsList = [...state.roomsList];
  switch (action.type) {
    case CREATE_ROOM_REQUEST:
      return {
        ...state,
        waitingToCreateRoom: true,
      };
    case GET_ROOMS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ADD_USER_TO_ROOM_REQUEST:
      return {
        ...state,
        waitingToAddMember: true,
      };
    case UPDATE_CURRENT_ROOM_SUCCESS:
      const {email, isCurrentRoom} = action.payload;
      const members = state.currentRoom.members;
      if (!isCurrentRoom) {
        return state;
      }
      for (const member of members) {
        if (member.email === email) {
          return state;
        }
      }
      return {
        ...state,
        currentRoom: {
          ...state.currentRoom,
          members: members.concat({email}),
        },
      };
    case GET_ROOM_SUCCESS:
      const shouldAdd = checkRoomInRoomsList(action.payload._id, state.roomsList);
      return {
        ...state,
        currentRoom: action.payload,
        roomsList: shouldAdd ? [action.payload].concat(roomsList) : roomsList,
      };
    case CREATE_ROOM_SUCCESS:
      return {
        ...state,
        waitingToCreateRoom: false,
        roomsList: [action.payload].concat(state.roomsList),
      };
    case ADD_USER_TO_ROOM_SUCCESS:
      for (const room of roomsList) {
        if (room._id === action.payload.roomID) {
          room.members = room.members.concat(action.payload.user);
        }
      }
      return {
        ...state,
        roomsList,
        waitingToAddMember: false,
      };
    case GET_ROOMS_SUCCESS:
      const {list, totalCount, cursor, shouldGetMore} = action.payload;
      let newRoomsList = [];

      if (!shouldGetMore) {
        newRoomsList = list;
      }
      if (shouldGetMore) {
        newRoomsList = state.roomsList.concat(list);
      }

      return {
        ...state,
        cursor,
        roomsList: newRoomsList,
        hasMore: newRoomsList.length < totalCount,
        loading: false,
      };
    case RECEIVE_MESSAGE_SUCCESS:
    case SEND_MESSAGE_SUCCESS:
      for (const room of roomsList) {
        if (room._id === action.payload.roomID) {
          room.lastMessage = action.payload;
        }
      }
      return {
        ...state,
        loading: false,
        roomsList,
      };
    case GET_ROOMS_FAILURE:
      return {
        ...state,
        loading: true,
      };
    default:
      return state;
  }
}

export function getRooms(shouldGetMore: boolean) {
  return (dispatch, getState) => {
    const cursor = getState().roomsList.cursor;
    let endpoint = `/rooms?limit=20&sortDirection=DESC&sortBy=createdAt`;

    if (shouldGetMore) {
      endpoint += `&cursor=${cursor}`;
    }

    return dispatch({
      [CALL_API]: {
        method: 'GET',
        endpoint,
        types: [
          GET_ROOMS_REQUEST,
          {
            type: GET_ROOMS_SUCCESS,
            payload: (_, __, res) => {
              return getJSON(res).then((json) => {
                return {
                  ...json,
                  shouldGetMore,
                };
              });
            },
          },
          GET_ROOMS_FAILURE,
        ],
      },
    });
  };
}

export function createRoom(data: any, userInRoom: any) {
  return {
    [CALL_API]: {
      method: 'POST',
      endpoint: `/rooms`,
      body: JSON.stringify(data),
      types: [
        CREATE_ROOM_REQUEST,
        {
          type: CREATE_ROOM_SUCCESS,
          payload: (_, __, res) => {
            return getJSON(res).then((json) => {
              return {
                ...json,
                members: userInRoom,
              };
            });
          },
        },
        CREATE_ROOM_FAILURE,
      ],
    },
  };
}

export function addUserToRoom(data: any, roomID: string) {
  return (dispatch, getState) => {
    const {_id, email} = getState().auth.user;

    return dispatch({
      [CALL_API]: {
        method: 'PUT',
        endpoint: `/rooms/${roomID}/members/add`,
        body: JSON.stringify(data),
        types: [
          ADD_USER_TO_ROOM_REQUEST,
          {
            type: ADD_USER_TO_ROOM_SUCCESS,
            payload: {
              roomID,
              user: [{_id, email}],
            },
          },
          ADD_USER_TO_ROOM_FAILURE,
        ],
      },
    });
  };
}

export function getRoom(roomId: string) {
  return {
    [CALL_API]: {
      method: 'GET',
      endpoint: `/rooms/${roomId}`,
      types: [
        GET_ROOM_REQUEST,
        GET_ROOM_SUCCESS,
        GET_ROOM_FAILURE,
      ],
    },
  };
}

const checkRoomInRoomsList = (roomId, roomsList) => {
  for (const room of roomsList) {
    if (room._id === roomId) {
      return false;
    }
  }

  return true;
};

export function updateCurrentRoom(data) {
  return {
    type: UPDATE_CURRENT_ROOM_SUCCESS,
    payload: data,
  };
};
