import { CALL_API, getJSON } from 'redux-api-middleware';
import { LOCATION_CHANGE } from 'react-router-redux';

export const SEARCH_USERS_REQUEST: string = 'users/SEARCH_USERS_REQUEST';
export const SEARCH_USERS_SUCCESS: string = 'users/SEARCH_USERS_SUCCESS';
export const SEARCH_USERS_FAILURE: string = 'users/SEARCH_USERS_FAILURE';

export const UPDATE_LAST_MESSAGE_SUCCESS: string = 'users/UPDATE_LAST_MESSAGE_SUCCESS';

export const RESET_USER: string = 'users/RESET_USER';

const initialState = {
  users: [],
  cursor: '',
  hasMore: false,
  loading: false,
};

export function usersListReducer(state = initialState, action) {
  switch (action.type) {
    case RESET_USER:
    case LOCATION_CHANGE:
      return {
        users: [],
        cursor: '',
        hasMore: false,
        loading: false,
      };
    case SEARCH_USERS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case SEARCH_USERS_SUCCESS:
      const {list, totalCount, cursor, shouldGetMore, userId} = action.payload;
      const filteredUsers = list.filter((item) => item._id !== userId);
      let newUsers = [];
      let hasMore = false;

      if (!shouldGetMore) {
        newUsers = filteredUsers;
        hasMore = list.length < totalCount;
      }
      if (shouldGetMore) {
        newUsers = state.users.concat(filteredUsers);
        hasMore = state.users.concat(list).length < totalCount;
      }

      return {
        ...state,
        cursor,
        users: newUsers,
        hasMore,
        loading: false,
      };
    default:
      return state;
  }
}

export function searchUsers(emailName: string, shouldGetMore: boolean) {
  return (dispatch, getState) => {
    const {users: {cursor: prevCursor}, auth: {user}} = getState();

    let endpoint = `/users?email=${emailName}&limit=20`;

    if (shouldGetMore) {
      endpoint += `&cursor=${prevCursor}`;
    }

    return dispatch({
      [CALL_API]: {
        method: 'GET',
        endpoint,
        types: [
          SEARCH_USERS_REQUEST,
          {
            type: SEARCH_USERS_SUCCESS,
            payload: (_, __, res) => {
              return getJSON(res).then((json) => {
                return {
                  ...json,
                  shouldGetMore,
                  userId: user._id,
                };
              });
            },
          },
          SEARCH_USERS_FAILURE,
        ],
      },
    });
  };
}

export function resetUsers() {
  return {
    type: RESET_USER,
  };
}
