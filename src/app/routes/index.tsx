import * as React from 'react';
import { Route, IndexRoute } from 'react-router';
import {
  App,
} from 'modules';
import { Chats } from 'modules/Chats';
import { Auth } from 'modules/Auth';

// Check and update these routes with ids

const AppComp = (props) => <App {...props} />;

export default (
  <Route>
    <Route path="/" component={AppComp}>
      <IndexRoute component={Chats} />
      <Route path="/rooms/:roomId" component={Chats} />
    </Route>

    <Route path="/auth" component={Auth} />
  </Route>
);
